FROM golang:alpine AS builder
RUN apk update && apk add --no-cache git
WORKDIR $GOPATH/src/mypackage/myapp/

ENV CGO_ENABLED=0
COPY . .

RUN go get -d -v
RUN go build -o /go/bin/main

FROM scratch
EXPOSE 8000
COPY --from=builder /go/bin/main /go/bin/main
ENTRYPOINT ["/go/bin/main"]